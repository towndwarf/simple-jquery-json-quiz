var questionBank=[];
var arrOrder=[];
var questionText="#question";
var description = '#description';
var descriptionVal='';
var opt1 = "#answer1";
var opt2 = "#answer2";
var opt3 = "#answer3";
var i, j, k;
var answers = [];

var delimeter =",";
var questionLock=false;
var numberOfQuestions;
var answer=[];
var expectedAnswer =[];
var score=0;
var questionNumber = 0;
$(document).ready(function(){
    $("#sampleButtonDiv").button();
    

    var score=0;
    /**
     * Randomize array element order in-place.
     * Using Durstenfeld shuffle algorithm.
     */
    function shuffleArray(arr) {
        for (var i = arr.length - 1; i > 0; i--) {
         var j = Math.floor(Math.random() * (i + 1));
         var temp = arr[i];
         arr[i] = arr[j];
         arr[j] = temp;
         }
        return arr;
    }
    var timer;
    function endAndStartTimer(funct, timeout ) {
        window.clearTimeout(timer);
        timer = window.setTimeout(funct,timeout);
    }
    $.getJSON('math.json', function(data) {
        arrOrder=[];


        delimeter = data.delimeter;
		descriptionVal = data.description;
		if(descriptionVal !== undefined)
			$(description).html(descriptionVal);
        for(k=0;k<data.quizlist.length;k++){
            questionBank[k] = [];
            questionBank[k][0]=data.quizlist[k].Q;
            questionBank[k][1]=[];

            for(j=0; j < data.quizlist[k].A.length; j++){
                questionBank[k][1][j] = [];
                questionBank[k][1][j][0] = data.quizlist[k].A[j].opt1;
                questionBank[k][1][j][1] = data.quizlist[k].A[j].opt2;
                questionBank[k][1][j][2] = (data.quizlist[k].A[j].var===undefined)?0:1; //mark if this answer is optional
            }
        }
        numberOfQuestions=questionBank.length;
        for(i=0; i< numberOfQuestions;i++)
            arrOrder[i]=i;

        arrOrder = shuffleArray(arrOrder);
        displayQuestion();
    });//gtjson


    function displayQuestion(){
		if (questionNumber >= numberOfQuestions) {
			displayFinalSlide();
			return;
		}
        $('.output').html('DONE '+(questionNumber) +' of ' + numberOfQuestions );
        console.log(questionBank[arrOrder[questionNumber]]);
        $(question).html('<div class="questionText">'+questionBank[arrOrder[questionNumber]][0]+'</div>');
        var str ='<input id="answ_0_0" class="option">'+delimeter+'<input id="answ_0_1" class="option">';
        $(opt1).html(str);
        str = '#answer'+(1);
        $(str).css("border", "0px solid white");
        $(str).css({"background":"white"});
        str = '#answer'+(2);
        $(str).css("border", "0px solid white");
		$(str).css("background","white");
        str = '#answer'+(3);
        $(str).css("border", "0px solid white");
        $(str).css("background","white");

        console.log(questionBank[arrOrder[questionNumber]]);
        //if(questionBank[arrOrder[questionNumber]][1].length >1) {
        str =  '<input id="answ_1_0" class="option">' + delimeter + '<input id="answ_1_1" class="option">';
            $(opt2).html(str);
        //}
        //if(questionBank[arrOrder[questionNumber]][1].length >2) {
        str = '<input id="answ_2_0" class="option">' + delimeter + '<input id="answ_2_1" class="option">';
         $(opt3).html(str);
        //}
        //setTimeout(function(){changeQuestion()},30000);

    }//display question


    function checkQuestion() {
		window.clearTimeout(timer);
		if (questionNumber <= numberOfQuestions) {
			expectedAnswer[questionNumber] = [];
			answer[questionNumber] = [];
			for (i = 0; i < questionBank[arrOrder[questionNumber]][1].length; i++) {
				expectedAnswer[questionNumber][i] = [];
				expectedAnswer[questionNumber][i][0] = questionBank[arrOrder[questionNumber]][1][i][0];
				expectedAnswer[questionNumber][i][1] = questionBank[arrOrder[questionNumber]][1][i][1];
				expectedAnswer[questionNumber][i][2] = questionBank[arrOrder[questionNumber]][1][i][2];
				answer[questionNumber][i] = [];
				answer[questionNumber][i][0] = false;
			}
			var result = true;
			for (i = 0; i < questionBank[arrOrder[questionNumber]][1].length; i++) {
				var inp = '#answ_' + i + '_0';
				answer[questionNumber][i][1] = $(inp).val().trim();
				inp = '#answ_' + i + '_1';
				answer[questionNumber][i][2] = $(inp).val().trim();
				str = '#answer' + (i + 1);
				console.log(str);
				$(str).css({"border": "red solid 3px"});
				$(str).css({"background": "red"});
				for (j = 0; j < questionBank[arrOrder[questionNumber]][1].length; j++) {

					var success = false;
					for (k = 0; !success && k < questionBank[arrOrder[questionNumber]][1].length; k++) {
						success =
							answer[questionNumber][i][1] == expectedAnswer[questionNumber][k][0]
							&& answer[questionNumber][i][2] == expectedAnswer[questionNumber][k][1]
							|| answer[questionNumber][i][1] == expectedAnswer[questionNumber][k][1]
							&& answer[questionNumber][i][2] == expectedAnswer[questionNumber][k][0];
					}
					answer[questionNumber][i][0] = success;
					if (success) {
						console.log(str);
						$(str).css({"border": "lightgreen solid 3px"});
						$(str).css({"background": "lightgreen"});
					}
				}
				result = result && answer[questionNumber][i][0];
			}
			score = score + result;
			if (result == false) {
				str = '<div style="color:green">';
				for (i = 0; i < questionBank[arrOrder[questionNumber]][1].length; i++) {
					str = str + expectedAnswer[questionNumber][i][0] + delimeter + expectedAnswer[questionNumber][i][1] + ' = ' + questionBank[arrOrder[questionNumber]][0] + '<br>';
				}
				str = str + '</div>';
				console.log(str);
				$('.output').html(str);
			}
			console.log('RESULT ' + result + ' | ' + answer[questionNumber] + ' || ' + expectedAnswer[questionNumber]);
			//console.log(expectedAnswer[questionNumber]);
			//console.log(questionBank[arrOrder[questionNumber]]);
			//answers[questionNumber] = answer;
			//if(answer[questionNumber][0] && undefined!==answer[questionNumber][1])
			questionNumber++;
			console.log('QUESTION:' + questionNumber + '/' + numberOfQuestions);
			if (questionNumber < numberOfQuestions) {
				timer = setTimeout(function () {
					displayQuestion()
				}, 5000);

			} else {
				displayFinalSlide();
			}
			;
		} else
			displayFinalSlide();
	}

		function displayFinalSlide(){
        //if(numberOfQuestions > score){
            $('.output').html('<div style="font-size: larger;color:orange">Ufff. Finished<br><br>Questions: '+numberOfQuestions+'Proper answers: '+score+'</div>');
			str = '<br><div style="color:red">' + "YOU'VE MISSED:<br>";
			count = 0;
			for( j=0; j < questionBank.length; j++) {
				if(answer[j][0][0]!=true){
					for( i=0; i < questionBank[arrOrder[j]][1].length; i++) {
						str = str + expectedAnswer[j][i][0] + delimeter + expectedAnswer[j][i][1] + ' = ' +questionBank[arrOrder[j]][0] +'<br>';
					}
					count++;
				}
				
			}
            if(count == 0){
				str = str +'</div>';
				console.log(str);
				$(".output").html('<div style="font-size: large;color:greenyellow">CONGRATULATUIONS!<br>You can be proud of yourself and kick some asses :)</div>');
			}else
				$('.output').append(str);
		/*}
        else
            
*/
    }//display final slide

    $("#sampleButtonDiv").click(function (evt) {
        // increment the value of output
        window.clearTimeout(timer);
        checkQuestion();

    });
});
