# README #

This is simple HTML + jQuery + JSON (as a DB dictionary) quiz system.
I created it as a helper to my son - study math.

it is VERY simplistic and requires future work
### What is this repository for? ###
It might be good for language /vocabulary studies 
It was created to handle very simple tasks:

 * gets questions and answers from flat JSON file 
 * asks questions and expects user's replies.
 * one must answer all the possible questions
 * if no proper answer(s) provided - the answer is shown below the 'continue' button.

# Quick summary
* Very simple dictionary based quiz system

# Version
0.1
### How do I get set up? ###

Take the files and put them somewhere - no website required.

Modify paths for jQuery and jQuery UI files (get latest versions)

Project runs as it is on FireFox, but requires web server for many other browsers

### Contribution guidelines ###

If anyone need this except for my 10 years old son - I'll be pleased to write it in a better way :)

### Who do I talk to? ###
* Repo owner